#include "go_luajit.h"

lua_err *internal_dostring(lua_State *_L, char *script) {
	int retVal = luaL_dostring(_L, script);
	return get_lua_error(_L, retVal);
}

// 加载并执行 Lua 脚本文件
lua_err *internal_dofile(lua_State *_L, const char *filename) {
    int retVal = luaL_dofile(_L, filename);
    return get_lua_error(_L, retVal);
}

lua_State *new_luajit_state() {
	lua_State *_L = luaL_newstate();
	luaL_openlibs(_L);

	luaL_newmetatable(_L, MT_GOCALLBACK);
	lua_pushliteral(_L,"__call");
	lua_pushcfunction(_L,&execute_go_callback);
	lua_settable(_L,-3);

	lua_pushliteral(_L,"__gc");
	lua_pushcfunction(_L,&release_cgo_handle);
	lua_settable(_L,-3);
	lua_pop(_L,1);

	init_pools(_L);

	return _L;
}

void close_lua(lua_State *_L) {
    free_pools(_L);
    lua_close(_L);
}
// 获取 Lua 状态机的使用内存
size_t get_lua_memory_usage(lua_State *_L) {
     // 调用 lua_gc 获取内存使用情况
     return lua_gc(_L, LUA_GCCOUNT, 0) * 1024 + lua_gc(_L, LUA_GCCOUNTB, 0);
 }