package luajitter

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"testing"
	"time"
)

// 测试运行lua脚本

func luaChipFun1(args []interface{}) ([]interface{}, error) {
	param1, ok := args[0].(float64)
	if !ok {
		return nil, errors.New("argument 1 to Add was not a number")
	}
	_ = param1
	param2, ok := args[1].(bool)
	if ok {
		return nil, errors.New("argument 2 to Add was not a number")
	}
	_ = param2
	// 第三个参数：期望为 map
	param3, ok := args[2].(*LocalLuaTable)
	if !ok {
		return nil, errors.New("argument 3 to luaChipFun1 was not a table")
	}
	mapParam, _ := param3.Unroll()
	// 处理 map 的内容
	fmt.Printf("Received table: %+v\n", mapParam)
	return []interface{}{
		interface{}(param1),
		interface{}(param2),
		interface{}(mapParam),
	}, nil
}
func TestScriptTransParams(t *testing.T) {
	clearAllocs()
	vm := NewState()
	defer vm.Close()

	err := vm.SetGlobal("_ChipFun1", luaChipFun1)
	if err != nil {
		panic(err)
	}

	err = vm.DoString(`
function get_int()
	return 111
end
function get_bool()
	return 1 == 1
end
function get_table()
    local obj = {}
	obj[1] = 1
	obj["2"] = "2"
	obj["YYYY"] = true
	return obj
end
function printTable(tbl, indent)
    if not indent then indent = 0 end
    for key, value in pairs(tbl) do
        -- 打印缩进
        local prefix = string.rep("    ", indent)
        if type(value) == "table" then
            print(prefix .. key .. ":")
            printTable(value, indent + 1)  -- 递归调用
        else
            print(prefix .. key .. ": " .. tostring(value))
        end
    end
end
function fib_fun(val) 
	-- 打印参数test
    print(string.format("1----- %s --------", val))
	return _ChipFun1(get_int(), get_bool(), get_table())
end 
function main(val)  
    data, data2, data3 = fib_fun(val) 
    print("2-------------", data)
    print("3-------------", data2)
	print("4-----------printTable--")
	printTable(data3)--这个是table 
    return data
end
`)
	if err != nil {
		panic(err)
	}

	funcObj, err := vm.GetGlobal("main")
	if err != nil {
		panic(err)
	}
	f := funcObj.(*LocalLuaFunction)
	out, err := f.Call(35)
	if err != nil {
		panic(err)
	}
	fmt.Println(cbCount)
	fmt.Println(out[0]) //这个返回值貌似没有用
}

func readLuaFile(filePath string) ([]byte, error) {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return []byte(""), err
	}
	return content, nil
}

func TestScriptTransParams2(t *testing.T) {

	clearAllocs()
	vm := NewState()
	defer vm.Close()

	file1 := "./lua/base.lua"
	file2 := "./lua/logic.lua"
	file3 := "./lua/gofunc.lua"
	file4 := "./lua/logic_1.lua"

	err := vm.DoFile(string(file1))
	if err != nil {
		log.Fatalf("Error executing base.lua: %v", err)
		panic(err)
	}

	err = vm.DoFile(string(file2))
	if err != nil {
		log.Fatalf("Error executing logic.lua: %v", err)
		panic(err)
	}
	err = vm.DoFile(string(file3))
	if err != nil {
		log.Fatalf("Error executing logic.lua: %v", err)
		panic(err)
	}
	bs, _ := readLuaFile(file4)
	if err := vm.LoadBuffer(bs, "logic_1.lua"); err != nil {
		fmt.Println("Error executing Lua script:", err)
		return
	}
	/*	err = vm.DoFile(string(file4))
		if err != nil {
			log.Fatalf("Error executing logic.lua: %v", err)
			panic(err)
		}*/

	err = vm.SetGlobal("_ChipFun1", luaChipFun1)
	if err != nil {
		panic(err)
	}

	printResultFn := func(result []interface{}, err error) {
		if err != nil {
			fmt.Printf(err.Error())
			return
		}
		fmt.Printf("Lua function executed successfully, results:%v \n", result)
		/*		if len(result) > 0 && result[0] != nil {
					fmt.Printf("Error calling Lua function:%v \n", result[0])
					return
				}
				fmt.Printf("Lua function executed successfully, results:%v \n", result[1])*/
	}
	/*func() {
		funcObj, err := vm.GetGlobal("function_test_failed")
		if err != nil {
			panic(err)
		}
		f := funcObj.(*LocalLuaFunction)
		result, err := f.Call(10000)
		printResultFn(result, err)
	}()*/
	func() {
		funcObj, err := vm.GetGlobal("call_function")
		if err != nil {
			panic(err)
		}
		f := funcObj.(*LocalLuaFunction)
		result, err := f.Call("function_test_failed2", 33)
		printResultFn(result, err)
	}()

	func() {
		// 正常返回
		funcObj, err := vm.GetGlobal("call_function")
		if err != nil {
			panic(err)
		}
		f := funcObj.(*LocalLuaFunction)
		out, err := f.Call("function_test_param1", 10000, "text1")
		if err != nil {
			panic(err)
		}
		printResultFn(out, err)
		//out, err = f.Call("function_test_add", 1, "two")
		//if err != nil {
		//	panic(err)
		//}
		params := map[interface{}]interface{}{"a100": 100, "101": 101, 102: 102, 103: 103}
		result, err := f.Call("main", params, "text")
		printResultFn(result, err)
	}()

	/*	func() {
		err = vm.SetGlobal("go_luaCallTest", luaCallTest)
		if err != nil {
			panic(err)
		}
		funcObj, err := vm.GetGlobal("call_function")
		if err != nil {
			panic(err)
		}
		f := funcObj.(*LocalLuaFunction)
		result, err := f.Call("testPanicMain_2", 33)
		printResultFn(result, err)
	}()*/
	func() {
		// 注册 Go 函数到 Lua
		/*vm2.functions["go_luaCallTest"] = luaCallTest

		// 获取 Lua 函数并调用
		funcObj := &LocalLuaFunction2{}
		f := funcObj
		result, err := f.Call("testPanicMain", 33)

		fn2 := func(result []interface{}, err error) {
			if err != nil {
				fmt.Printf("Error calling Lua function: %v \n", err)
				return
			}
			if len(result) > 1 {
				fmt.Printf("Error calling Lua function: %v \n", result[1])
				return
			}
			fmt.Printf("Lua function executed successfully, results: %v \n", result[0])
		}
		fn2(result, err)*/

		err = vm.SetGlobal("go_luaCallTest", luaCallTest)
		if err != nil {
			panic(err)
		}
		funcObj, err := vm.GetGlobal("call_function")
		if err != nil {
			panic(err)
		}
		f := funcObj.(*LocalLuaFunction)
		result, err := f.Call("testPanicMain", 33)
		// 获取返回值
		/*		if result != nil && len(result) > 0 {
					if luaError, ok := result[0].(string); ok && luaError != "" {
						// 打印 Lua 返回的错误信息
						fmt.Printf("Lua error: %s\n", luaError)
					} else {
						fmt.Printf("Lua function executed successfully, result: %v\n", result[0])
					}
				} else {
					fmt.Println("Lua function returned nil")
				}*/
		printResultFn(result, err)
	}()

}

func luaCallTest(args []interface{}) ([]interface{}, error) {
	if len(args) > 0 {
		return nil, errors.New("fun:luaCallTest   test Panic") // 返回错误
	}
	return []interface{}{interface{}(1)}, nil
}

func TestScriptTransParams3(t *testing.T) {
	// 创建 Lua 状态机

	clearAllocs()
	vm := NewState()
	defer vm.Close()
	// 加载 Lua 脚本
	luaScript := `
        function errorHandler1(err)
            print("Error: " .. tostring(err))
            return err  -- 返回错误信息
        end

        function potentiallyFailingFunction1(s)
            print("potentiallyFailingFunction1-------------", s)
            local a = 1
            local c = "string"  -- 故意使用字符串
            return a + c  -- 这会引发错误
        end

        function function_test_failed(a)
            local success, result = xpcall(function()
                return potentiallyFailingFunction1(a)
            end, errorHandler1)

            if not success then
                return  nil, result  -- 返回 nil 和错误信息
            else
                return result  -- 返回成功的结果
            end
        end
    `

	// 执行 Lua 脚本
	if err := vm.DoString(luaScript); err != nil {
		fmt.Println("Error executing Lua script:", err)
		return
	}

	// 调用 Lua 函数
	funcObj, err := vm.GetGlobal("function_test_failed")
	if err != nil {
		panic(err)
	}
	f := funcObj.(*LocalLuaFunction)

	// Call the function and check for errors
	results, err := f.Call(10000)
	if err != nil {
		fmt.Println("Error calling Lua function:", err)
	} else {
		if len(results) > 1 {
			fmt.Println("Error calling Lua function:", results[1])
			return
		}
		fmt.Println("Lua function executed successfully, results:", results[0])
	}
}

func TestScriptTransParaAsync(t *testing.T) {

	clearAllocs()
	vm := NewState()
	defer vm.Close()

	file1 := "./lua/base.lua"
	file2 := "./lua/logic.lua"
	file3 := "./lua/gofunc.lua"
	file4 := "./lua/logic_async.lua"

	err := vm.DoFile(string(file1))
	if err != nil {
		log.Fatalf("Error executing base.lua: %v", err)
		panic(err)
	}

	err = vm.DoFile(string(file2))
	if err != nil {
		log.Fatalf("Error executing logic.lua: %v", err)
		panic(err)
	}
	err = vm.DoFile(string(file3))
	if err != nil {
		log.Fatalf("Error executing logic.lua: %v", err)
		panic(err)
	}
	bs, _ := readLuaFile(file4)
	if err := vm.LoadBuffer(bs, "logic_async.lua"); err != nil {
		fmt.Println("Error executing Lua script:", err)
		return
	}
	printResultFn := func(result []interface{}, err error) {
		if err != nil {
			fmt.Printf(err.Error())
			return
		}
		fmt.Printf("Lua function executed successfully, results:%v \n", result)
	}
	func() {
		funcObj, err := vm.GetGlobal("call_function")
		if err != nil {
			panic(err)
		}
		f := funcObj.(*LocalLuaFunction)
		result, err := f.Call("testAsync", 33, 111)
		printResultFn(result, err)
	}()
	time.Sleep(100000)
}
