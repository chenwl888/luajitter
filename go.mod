module gitee.com/chenwl888/luajitter

go 1.18

require (
	github.com/baohavan/go-pointer v0.0.0-20181113050700-6f48d0300d21
	github.com/stretchr/testify v1.9.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
