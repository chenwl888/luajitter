
function get_int()
	return 111
end
function get_bool()
	return 1 == 1
end
function get_table()
    local obj = {}
	obj[1] = 1
	obj["2"] = "2"
	obj["YYYY"] = true
	return obj
end
function printTable(tbl, indent)
    if not indent then indent = 0 end
    for key, value in pairs(tbl) do
        -- 打印缩进
        local prefix = string.rep("    ", indent)
        if type(value) == "table" then
            print(prefix .. key .. ":")
            printTable(value, indent + 1)  -- 递归调用
        else
            print(prefix .. key .. ": " .. tostring(value))
        end
    end
end
function fib_fun(val)
	-- 打印参数test
    print(string.format("1----- %s --------", val))
	return _ChipFun1(get_int(), get_bool(), get_table())
end

function main(arg, context)
    print("input--1-------------", arg)
	printTable(arg)
    print("input--2-------------", context)
    print("input--3-------------", arg["a100"])
    error("test main err")

--[[    local success, data, data2, data3 = xpcall(function()
        return fib_fun(arg)  -- 直接调用 fib_fun(arg)
    end, errorHandler1)
    if not success then
        print("----------------------------------------")
        return nil, data, data2  -- 或者返回适当的值
    else
        return data, data2, data3  -- 正确返回结果
    end]]

    --data, data2, data3 = fib_fun(arg)
    --print("2-------------", data)
    --print("3-------------", data2)
	--print("4-----------printTable--")
	--printTable(data3)--这个是table
    --return data
end

function function_test_param1(val, context)
    print("function_test_param1--1-------------", val)
    print("function_test_param1--2-------------", context)
    return
end


-- 以下是测试lua报错堆栈信息
function errorHandler1(err)
    local trace = debug.traceback()  -- 获取调用栈信息
    print("#Error: " .. tostring(err))
    print("Stack Trace:\n" .. trace)
    return err  -- 返回错误信息
end


function potentiallyFailingFunction1(s)
    print("potentiallyFailingFunction1-------------", s)
    -- 故意制造一个错误
    local a = 1
    local c = "string"  -- 这里故意使用字符串
    return a + c  -- 这会引发错误
    --error("An intentional error occurred!")  -- 故意制造错误
end

function function_test_failed(a )-- 这个potentiallyFailingFunction1内部报错不会返回到go错误
    -- 使用 xpcall 捕获错误
    local success, result = xpcall(function(a)
        return potentiallyFailingFunction1(a)
    end, errorHandler1, a)  -- 将 ... 传递给 xpcall
    if not success then
        print("LuaFunction execution failed22: " .. tostring(result))
        return nil, tostring(result)
    else
        return nil
    end
end


function function_test_failed2(s) --- 这个错误会直接返回给golang
    print("function_test_failed2-------------", s)
    -- 故意制造一个错误
    local a = 1
    local c111 = "string"  -- 这里故意使用字符串
    return a + c111  -- 这会引发错误
    --error("An intentional error occurred!")  -- 故意制造错误
end



function errorHandler11(err)
    if err then  -- 判断 err 是否不为空
        local trace = debug.traceback()  -- 获取调用栈信息
        print("#Error332: " .. tostring(err))
        print("Stack Trace:\n" .. trace)
    else
        return nil
    end

    local out_string = ""
    local stackLevel = 4  -- 指定要获取的栈级别
    local info = debug.getinfo(stackLevel)
    if info and info.name then
        local func_name = info.name
        local file_source = info.source
        if file_source:sub(1, 1) == "@" then
            file_source = file_source:sub(2)  -- 去掉开头的 @ 符号
        end
        local current_line = info.currentline
        out_string = file_source..":"
        out_string = out_string ..current_line..":"
        out_string = out_string.." (function : \"" .. func_name.."\" )"
        out_string = out_string.." (go err : \"" .. tostring(err).."\" )"
    end
    print(out_string)
    return out_string -- 返回错误信息作为字符串

--[[    local info2 = debug.getinfo(3, "Sl")
    if type(info2) == "table" then
        print("Debug info:", info2)
        for key, value in pairs(info2) do
            local status, result = pcall(function() return value end)
            if status then
                print(key .. ": " .. tostring(result))
            else
                print(key .. ": Unable to access value")
            end
        end
    else
        print("No debug info available.")
    end]]
end

function testPanicMain_2(arg, context)
    local ret, ret2 =  testPanic(val)
    print("testPanicMain-------------", ret,ret2)
    if ret == nil then
        --error("gofunction err:"..tostring(ret2))
    else
        print("ret is not nil, value: ", ret)
    end
    return tostring(ret2), tostring(ret2)
end
function testPanic(val)
    local success, data, data2 = xpcall(function()
        return go_luaCallTest(val)  -- 在这里调用 testPanic
    end, errorHandler11)
    if not success then
        print("nil, data, data2-------------", data, data2 )
        return nil, data, data2  -- 返回 nil 和错误信息
    else
        print("data, data2-------------")
        return data, data2
    end
    --error("3333 error occurred!")  -- 故意制造错误
end
reg_lua_function("testPanicMain_2", testPanicMain_2)
reg_lua_function("main", main)
reg_lua_function("function_test_param1", function_test_param1)
reg_lua_function("function_test_failed", function_test_failed)
reg_lua_function("function_test_failed2", function_test_failed2)

