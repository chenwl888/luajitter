
local goFunctions = {}

function reg_cmd(fun_name, func)
    goFunctions[fun_name] = func
end

local luaFunctions = {}
function reg_lua_function(name, func)
    luaFunctions[name] = func
end

function errorHandler(err)
    print("@------外层----Error: " .. tostring(err))
--[[     if not debug then
        error("Debug library is not available.")
    else
        print("Debug library is available.")
    end ]]
    --print(debug.traceback())
                   print(debug.traceback())
    local info = debug.getinfo(3, "Sl")
    print("#Error occurred in: " .. info.source .. " at line " .. info.currentline)

    return err
end
function traceError(err)
    return error("exec lua err:".. tostring(err))
end
--[[
    输入:         name 调用函数名, ... 形参
    输出结果:      参数1:nil(不报错)/报错文本, 参数2:返回结果
]]
function call_function(name, ...)
    local func = luaFunctions[name]
    if func then
        -- 使用 xpcall 捕获错误，传递可变参数
        local success, result, result2  = xpcall(function(...)
            return func(...)  -- 这里将 ... 传递给 func
        end, errorHandler, ...)  -- 将 ... 传递给 xpcall

        --print(tostring("==================== '" .. tostring(result) .. "'  ".. tostring(success)))
        if not success then
            --print(tostring("11111111111 lua本体报错" ))
            -- 这里的 err 是 xpcall 返回的第二个值
            if result ~= nil then
                traceError(result)
            else
                error("exec lua err: unknown error" )
            end
            --return tostring(result)
       else
--[[            --print(tostring("222222222222 可能正常，可能go报错" ))
            if result ~= nil then
                --print(tostring("33333333333333 go报错" ))
                return tostring(result)
            end
            --print(tostring("4444444444444444444444444444444444444" ))
            return nil, result2  -- 返回正常结果]]
       end
--[[        local success, err = pcall(function(...)
            return func(...)  -- 这里将 ... 传递给 func
        end, errorHandler, ...)
        if not success then
            errorHandler(err)
        end]]
    else
        error("LuaFunction '" .. name .. "' does not exist.")
        --return tostring("LuaFunction '" .. name .. "' does not exist.")
    end
end

local goFunctions = {}
function reg_go_function(name, func)
    goFunctions[name] = func
end
function cmd_function(name, ...)
    local func = goFunctions[name]
    print("cmd_function------------111", name)
    if func then
        local success, data, data2 = xpcall(function()
            return func(val)  -- 在这里调用 testPanic
        end, cmd_errorHandler)
        if not success then
            --print("cmd_function------------333", data, data2)
            --print("nil, data, data2-------------", data, data2 )
            error("["..tostring(data)..tostring(data2).."]")
            return nil, data, data2  -- 返回 nil 和错误信息
        else
            --print("cmd_function------------444", data, data2)
            --print("data, data2-------------")
            return data, data2
        end
    end
end

function cmd_errorHandler(err)
--[[    if err then  -- 判断 err 是否不为空
        local trace = debug.traceback()  -- 获取调用栈信息
        print("#Error542: " .. tostring(err))
        print("Stack Trace:\n" .. trace)
    else
        return nil
    end]]

    --print("getinfo---------------------------\n")
    local out_string = ""
    local stackLevel = 6  -- 指定要获取的栈级别
    local info = debug.getinfo(stackLevel, "nSl")
    --[[stackLevel = 1
    while true do
        local info = debug.getinfo(stackLevel, "nSl")
        if not info then
            break
        end

        -- 打印当前栈级别的信息
        print(string.format("Level %d: %s in %s:%d",
                stackLevel,
                tostring(info.name or "unknown"),
                info.source,
                info.currentline))

        -- 检查是否到达目标行
        if stackLevel > 11 then
            break
        end

        stackLevel = stackLevel + 1
    end]]
--[[    if info then
        print("Function Name: " .. tostring(info.name or "unknown"))
        print("Source: " .. tostring(info.source))
        print("Line: " .. tostring(info.currentline))
    else
        print("No info available for stack level " .. stackLevel)
    end]]
    if info  then
        local func_name = info.name or "unknown"
        local file_source = info.source
        if file_source:sub(1, 1) == "@" then
            file_source = file_source:sub(2)  -- 去掉开头的 @ 符号
        end
        local current_line = info.currentline
        out_string = file_source..":"
        out_string = out_string ..current_line..":"
        out_string = out_string.." (function : \"" .. func_name.."\" )"
        out_string = out_string.." (go err : \"" .. tostring(err).."\" )"
    end
    --print(out_string)
    return out_string -- 返回错误信息作为字符串
end