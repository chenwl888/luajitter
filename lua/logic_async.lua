--- 业务代码测试
---
function testAsync(arg)
    print("----------testAsync")

    local event2 = "Test Event"
    local context2 = { name = "Test Context" }
    CallTextFunc("myFunction", context2, event2)
    CallTextFunc("myFunction2", context2, event2)

    -- 创建上下文
    local event = "Test Event"
    local context1 = { name = "Test Context" }
    runCoroutine(context1, event)
    -- 模拟一些时间延迟后继续协程
    print("---------- Simulating delay before resuming coroutine...")
    --os.execute("sleep 1")  -- 在某些环境中使用 os.execute("sleep 1")

    -- 继续执行协程
    --context.call(context)  -- 这里将继续从 coroutine.yield 处恢复
end
function myFunction(context, event)
    print("---------- myFunction: " .. context.name)
    print("Function executed after 1 second delay.")
end

function myFunction2(context, event)
    print("---------- myFunction2: " .. context.name)
    print("Function executed after 1 second delay.")
end

-- 根据字符串名称获取对应的函数
function getfunctionbystring(func_name)
    return _G[func_name]  -- 从全局环境中获取函数
end

function CallTextFunc(func_name, context, event)
    if func_name ~= "" then
        local func = getfunctionbystring(func_name)
        if type(func) == "function" then
            func(context, event)
        else
            print("Function '" .. func_name .. "' not found.")
        end
    end
end



function delay(seconds)
    print("----------delay "..tostring(seconds).. ("s start"))
    local start = os.clock()
    local index = 0
    while os.clock() - start < seconds do
        if math.floor((os.clock() - start) *10) > index then
            index = math.floor((os.clock() - start) *10)
            --print("---------- delay --- step"..tostring(index).. ("----") ..tostring(os.clock()))
        end
    end
    print("----------delay "..tostring(seconds).. ("s end"))
end

-- 执行协程
function runCoroutine(context, event)
    print("---------- start runCoroutine")
    context.routine = coroutine.create( function(ctx, e)
        print("---------- coroutine.create  ".. ctx.name)
        delay(3)  -- 延迟 1 秒
        -- 模拟异步工作
        for i = 1, 5 do
            print("---------- Working in coroutine... " .. i)
            -- 在这里模拟某种条件来 yield
            if i == 2 then
                print("---------- coroutine.yield 2")
                ctx:wait()  -- 这里会阻塞并暂停协程
                ctx:testfunc()
            end
        end
        print("---------- coroutine.create  ".. ctx.name)
    end)
    context.call = function(ctx, ret, err)
        print("---------- coroutine.resume")
       -- local status, err = coroutine.resume(ctx.routine, ctx, ret, err)
        local status, err = coroutine.resume(ctx.routine, ctx, ret, err)
        if not status then
            print("Error during coroutine execution: " .. err)
        end
    end
    context.wait = function()
        print("---------- coroutine.yield")
        return coroutine.yield()
    end
    context.testfunc = function()
        print("----------testfunc-------------")
    end
    print("---------- start coroutine.resume ")
    local status, err = coroutine.resume(context.routine, context, event)  -- 启动协程
    if not status then
        print("Error starting coroutine: " .. err)
    end
    print("----------running  coroutine.resume ")
    context.call(context)
--[[    while coroutine.status(co) ~= "dead" do
        coroutine.resume(co)  -- 恢复协程执行
        os.execute("sleep 1")  -- 模拟等待，便于观察输出
    end]]
end

reg_lua_function("testAsync", testAsync)